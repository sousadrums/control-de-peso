<?php require_once 'includes/redirect.php'; ?>
<?php require_once 'includes/cabecera.php'; ?>
<?php require_once 'includes/helpers.php'; ?>

<div id="principal">
	<h1>Editar datos de usuario</h1>
	
	<?php if (isset($_SESSION['completado'])) : ?>
		<div class="alerta alerta-exito"><?=$_SESSION['completado']?></div>
	<?php elseif(isset($_SESSION['errors']['general'])): ?>
		<div class="alerta alerta-error"><?=$_SESSION['errors']['general']?></div>
	<?php endif; ?>
	<form action="update-user.php" method="POST" >
		<label for="name">Nombre: </label>
		<input type="text" name="name" value="<?=$_SESSION['usuario']['name']; ?>"/>
		<?php echo isset($_SESSION['errors']) ? mostrarError($_SESSION['errors'], 'name') : ''; ?>
		<br/>
		<label for="surname">Apellidos: </label>
		<input type="text" name="surname" value="<?=$_SESSION['usuario']['surname']; ?>"/>
		<?php echo isset($_SESSION['errors']) ? mostrarError($_SESSION['errors'], 'surname') : ''; ?>
		<br/>
		<label for="email">Email: </label>
		<input type="email" name="email" value="<?=$_SESSION['usuario']['email']; ?>"/>
		<?php echo isset($_SESSION['errors']) ? mostrarError($_SESSION['errors'], 'email') : ''; ?>
		<br/>
		<input type="submit" value="Actualizar" name="submit" />

	</form>
	<?php borrarErrores(); ?>



</div>



<?php require_once 'includes/footer.php'?> 