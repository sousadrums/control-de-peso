<?php 

if(isset($_POST)){
	
	require_once 'includes/conexion.php';

	//recoger los valores del post en variables
	$name= isset($_POST['name']) ? mysqli_real_escape_string($db, $_POST['name']) :false;
	$surname=isset($_POST['surname']) ? mysqli_real_escape_string($db, $_POST['surname']) :false;
	$email=isset($_POST['email']) ? mysqli_real_escape_string($db, trim($_POST['email'])) :false;
	
	
	//array de errores
	$errors=array();	

	//validar datos
	// validar nombre
	if (!empty($name) && !is_numeric($name) && !preg_match("/[0-9]/", $name)) {
		$name_validate= true;
	}else{
		$name_validate= false;
		$errors['name']= "el nombre no es valido";
	}
	// validar apellidos
	if (!empty($surname) && !is_numeric($surname) && !preg_match("/[0-9]/", $surname)) {
		$surname_validate= true;
	}else{
		$surname_validate= false;
		$errors['surname']= "los apellidos no son validos";
	}
	// validar correo
	if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$email_validate= true;
	}else{
		$email_validate= false;
		$errors['email']= "el correo no es valido";
	}
	


	$save_user=false;
	if (count($errors) == 0) {
		$usuario=$_SESSION['usuario'];
		$save_user=true;

// comprobar que el email no existe

		$sql="SELECT user_id, email FROM users WHERE email='$email'";
		$isset_email=mysqli_query($db, $sql);
		$isset_user= mysqli_fetch_assoc($isset_email);
		if ($isset_user['user_id']== $usuario['user_id'] || empty($isset_user)) {
		
		
			//actualizar usuario en bd

			$usuario=$_SESSION['usuario'];
			$sql="UPDATE users SET ".
			"name = '$name', ".
			"surname = '$surname', ".
			"email = '$email' ".
			"WHERE user_id = ".$usuario['user_id']; 
			$query=mysqli_query($db, $sql);
			
		
			if ($query) {
				$_SESSION['usuario']['name']= $name;
				$_SESSION['usuario']['surname']=$surname;
				$_SESSION['usuario']['email']=$email;
				$_SESSION['completado']="la actualización se ha completado con éxito";
			}else{
				$_SESSION['errors']['general']= "fallo al modificar el usuario";
			}
		}else{
		$_SESSION['errors']['general']= "el usuario ya existe";
		}


	}else{
		$_SESSION['errors']=$errors;	
	}

}
header('Location: userdata.php');
?>