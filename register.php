<?php 

if(isset($_POST)){
	if(!isset($_SESSION)){
		session_start();
	}
	require_once 'includes/conexion.php';

	$sql= "CREATE TABLE IF NOT EXISTS users(
		user_id int(255) auto_increment not null,
		name varchar(50),
		surname varchar(255),  
		email varchar(255), 
		password varchar(255), 
		fecha date NOT NULL DEFAULT CURRENT_TIMESTAMP,
		CONSTRAINT pk_users PRIMARY KEY (user_id)
	);";

	$create_usuarios_table= mysqli_query($db, $sql);
	
	//recoger los valores del post en variables
	$name= isset($_POST['name']) ? mysqli_real_escape_string($db, $_POST['name']) :false;
	$surname=isset($_POST['surname']) ? mysqli_real_escape_string($db, $_POST['surname']) :false;
	$email=isset($_POST['email']) ? mysqli_real_escape_string($db, trim($_POST['email'])) :false;
	$password=isset($_POST['password']) ? mysqli_real_escape_string($db, $_POST['password']) :false;
	
	//array de errores
	$errors=array();	

	//validar datos
	// validar nombre
	if (!empty($name) && !is_numeric($name) && !preg_match("/[0-9]/", $name)) {
		$name_validate= true;
	}else{
		$name_validate= false;
		$errors['name']= "el nombre no es valido";
	}
	// validar apellidos
	if (!empty($surname) && !is_numeric($surname) && !preg_match("/[0-9]/", $surname)) {
		$surname_validate= true;
	}else{
		$surname_validate= false;
		$errors['surname']= "los apellidos no son validos";
	}
	// validar correo
	if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$email_validate= true;
	}else{
		$email_validate= false;
		$errors['email']= "el correo no es valido";
	}
	// validar contraseña
	if (!empty($password)) {
		$password_validate= true;
	}else{
		$password_validate= false;
		$errors['password']= "el password no es valido";
	}


	$save_user=false;
	if (count($errors) == 0) {
		$save_user=true;

		//cifrado de contraseña en bbdd
		$password_safe=password_hash($password, PASSWORD_BCRYPT, ['cost'=>4]);
	
		//Insertar usuario en bd

		$sql= "INSERT INTO users VALUES(null, '$name', '$surname', '$email', '$password_safe', CURDATE());"; 
		$query=mysqli_query($db, $sql);
		
	
		if ($query) {
			$_SESSION['completado']="El registro se ha completado con éxito";
		}else{
			$_SESSION['errors']['general']= "fallo al insertar el usuario";
		}



	}else{
		$_SESSION['errors']=$errors;
		
	}
}
header('Location: index.php');
?>