<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Control de peso</title>
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
</head>
<body>
	<!--Cabecera-->
	<header id="header">	
		<!--logo-->
		<div id="logo">
			<a href="index.php">
				<h1>Control de peso Laura y Jorge</h1>
			</a>
		</div>
	</header>	
	<hr/>
	<div id="container">
		
		<!--contenido -->
<?php if(isset($_SESSION['usuario'])): ?>
	<div id="usuario-loggueado" class="block">
		<h3>bienvenido, <?=$_SESSION['usuario']['name'].' '.$_SESSION['usuario']['surname'];?></h3>
	<!--Botones-->
	<a href="cerrar.php" class="boton">Cerrar sesion</a>
	<a href="userdata.php" class="boton">mis datos</a>
	<a href="introducir.php" class="boton">introducir peso</a>
	<a href="ver.php" class="boton">ver peso</a>
	</div>
<?php endif; ?>