<?php
function mostrarError($errors, $field){
	$alert='';
	if (isset($errors[$field]) && !empty($field)) {
		$alert="<div class='alerta alerta-error'>".$errors[$field].'</div>';
	}
	return $alert;
}

function borrarErrores(){
	$borrado=false;
	if (isset($_SESSION['errors'])) {
		$_SESSION['errors']=null;
		$borrado= true;
	}

	if (isset($_SESSION['completado'])) {
		$_SESSION['completado']=null;
		$borrado=true;
	}
	
	return $borrado;
}
?>