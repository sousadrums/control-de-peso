<?php session_start(); ?>
<?php require_once 'includes/cabecera.php'; ?>
<?php require_once 'includes/helpers.php'; ?>





	<?php if(isset($_SESSION['error_login'])): ?>
	<div class="alerta alerta-error">
		<?=$_SESSION['error_login']; ?>
	</div>
<?php endif; ?>

<?php if(!isset($_SESSION['usuario'])): ?>
	<div id="login" class="block">
	<h3>Identifícate</h3>
	<form action="login.php" method="POST" >
		<label for="email">Email: </label>
		<input type="email" name="email" />
		<br/>
		<label for="password">Contraseña: </label>
		<input type="password" name="password"  />
		<br/>
		<input type="submit" value="Entrar" />

	</form>
</div>
<div id="register" class="block">
	<h3>Regístrate</h3>

	<?php if (isset($_SESSION['completado'])) : ?>
			<div class="alerta alerta-exito"><?=$_SESSION['completado']?></div>
		<?php elseif(isset($_SESSION['errors']['general'])): ?>
			<div class="alerta alerta-error"><?=$_SESSION['errors']['general']?></div>
		<?php endif; ?>
	<form action="register.php" method="POST" >
		<label for="name">Nombre: </label>
		<input type="text" name="name" />
		<?php echo isset($_SESSION['errors']) ? mostrarError($_SESSION['errors'], 'name') : ''; ?>
		<br/>
		<label for="surname">Apellidos: </label>
		<input type="text" name="surname" />
		<?php echo isset($_SESSION['errors']) ? mostrarError($_SESSION['errors'], 'surname') : ''; ?>
		<br/>
		<label for="email">Email: </label>
		<input type="email" name="email" />
		<?php echo isset($_SESSION['errors']) ? mostrarError($_SESSION['errors'], 'email') : ''; ?>
		<br/>
		<label for="password">Contraseña: </label>
		<input type="password" name="password" />
		<?php echo isset($_SESSION['errors']) ? mostrarError($_SESSION['errors'], 'password') : ''; ?>
		<br/>
		<input type="submit" value="Registro" name="submit" />

	</form>
	<?php borrarErrores(); ?>
</div>
<?php endif; ?>	
<?php require_once 'includes/footer.php'?>